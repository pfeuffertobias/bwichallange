/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.main;

import de.cmtmp.bwichallenge.verarbeitung.*;


/**
 * Main-Klasse - Startet die Verarbeitung
 *
 * @author tobias
 */
public class Main {

    /**
     * Start die Auswertung der Testdaten
     *
     * @param args Keine Übergabeparameter werden benötigt & beachtet
     */
    public static void main(String[] args) {
        new Verarbeitung().starteTests();
    }

}
