/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung;

import java.util.*;
import java.util.stream.*;


/**
 * Beladeliste aller Fahrzeuge
 *
 * @author tobias
 */
public class Beladeliste {

    private final ArrayList<Fahrzeugbeladung> beladung = new ArrayList<>(10);

    /**
     * Speichert das Ergebnis der Auswertung und kann es mit der toString Methode ausgeben
     */
    public Beladeliste() {
    }

    /**
     * Fügt das Ergebnis der Beladung für ein Fahrzeug hinzu
     *
     * @param beladung Die Fahrzeugbeladung, die zu dieser Beladeliste hinzugefügtt werden soll
     */
    public void addFahrzeugBeladung(Fahrzeugbeladung beladung) {
        this.beladung.add(beladung);
    }

    @Override
    public String toString() {
        return "\n\nBeladeListe hat den Gesamtnutzwert " + getIndex() + "\n\n" + beladung.stream().map(fnctn -> fnctn.toString()).collect(Collectors.joining("\n\n\n"));
    }


    /**
     * Gibt den transportierten Gesamtwert der Güter auf dieser Beladeliste zurück
     *
     * @return Der Gesamtwert der transportierten Güter
     */
    public double getIndex() {
        return beladung.stream().collect(Collectors.summingDouble(c -> c.getIndex()));
    }
}
