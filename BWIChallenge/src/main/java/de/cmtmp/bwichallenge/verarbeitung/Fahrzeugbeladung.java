/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung;

import de.cmtmp.bwichallenge.dataInput.*;
import java.util.*;
import java.util.stream.*;


/**
 * Beladeliste eines Fahrzeuge
 *
 * @author tobias
 */
public class Fahrzeugbeladung {

    private final String bemerkung;
    private final ArrayList<HardwareDataStorage> beladung = new ArrayList<>(10);

    /**
     * Erzeugt ein neues Fahrzeugbeladungsobjekt mit der nähren Beschreibung paramBemerkung
     *
     * @param paramBemerkung Bemerkung nähere Beschreibung der Fahrzeugbeladung
     */
    public Fahrzeugbeladung(String paramBemerkung) {
        this.bemerkung = paramBemerkung;
    }

    /**
     * Fügt eine Hardwareart der Beladung hinzu
     *
     * @param beladung Die hinzuzufügende Hardwareart
     */
    public void addBeladung(HardwareDataStorage beladung) {
        this.beladung.add(beladung);
    }

    @Override
    public String toString() {
        return "Fahrzeug: " + bemerkung + "\n\n\t" + beladung.stream().map(fnctn -> fnctn.toString()).collect(Collectors.joining("\n\t"));
    }

    /**
     * Gibt den transportierten Gesamtwert der Güter auf diesem Fahrzeugs zurück
     *
     * @return Der Gesamtwert der transportierten Güter des Fahrzeugs
     */
    public double getIndex() {
        return beladung.stream().collect(Collectors.summingDouble(c -> c.getAnzahl() * c.getNutzwert()));
    }


}
