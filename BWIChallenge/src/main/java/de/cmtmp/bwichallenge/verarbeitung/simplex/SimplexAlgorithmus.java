/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung.simplex;

import de.cmtmp.bwichallenge.dataInput.*;
import de.cmtmp.bwichallenge.verarbeitung.*;
import java.util.*;
import java.util.stream.*;


/**
 *
 * @author tobias
 */
public class SimplexAlgorithmus {

    SimplexTabelle data;

    public SimplexAlgorithmus(List<GewichtDataStorage> lg, DataInput dataInput) {

        List<HardwareDataStorage> hardware = dataInput.getHardwareData().collect(Collectors.toList());

        //Erzeugung lesbarer VariabelNamen (NichtBasisVariablen) für die Klasse SimplexAlgorithmus
        List<String> variablennamen = new LinkedList<String>();
        for (int localI = 0; localI < hardware.size(); localI++) {
            for (int localJ = 0; localJ < lg.size(); localJ++) {
                variablennamen.add(hardware.get(localI).toString() + "\t\t" + lg.get(localJ).toString());
            }

        }


        //Erzeugen einer für die Klasse SimplexAlgorithmus veraarbeitbaren datentabelle
        List<double[]> datentabelle = new LinkedList<>();

        double maximierung[] = new double[hardware.size() * lg.size() + 1];
        Arrays.fill(maximierung, 0);


        for (int localI = 0; localI < hardware.size() * lg.size(); localI += lg.size()) {
            double d[] = new double[hardware.size() * lg.size() + 1];
            Arrays.fill(d, 0);
            for (int localJ = 0; localJ < lg.size(); localJ++) {
                d[localI + localJ] = 1;
                maximierung[localI + localJ] = hardware.get(localI / lg.size()).getNutzwert();
            }
            d[d.length - 1] = hardware.get(localI / lg.size()).getAnzahl();
            datentabelle.add(d);
        }

        for (int localI = 0; localI < lg.size(); localI++) {
            double d[] = new double[hardware.size() * lg.size() + 1];
            Arrays.fill(d, 0);
            for (int localJ = 0; localJ < hardware.size(); localJ++) {
                d[localI + localJ * lg.size()] = hardware.get(localJ).getGewicht();
            }
            d[d.length - 1] = lg.get(localI).getGewicht();
            datentabelle.add(d);
        }

        datentabelle.add(maximierung);

        //Umformen der List<double[]> in double[][]
        double[][] tabelle = new double[datentabelle.size()][];
        for (int i = 0; i < datentabelle.size(); i++) {
            tabelle[i] = datentabelle.get(i);
        }


        data = new SimplexTabelle(variablennamen.toArray(), tabelle);

        starteVerarbeitung();
    }

    public void starteVerarbeitung() {
        while (data.isAUnabhängigeVariablePositiv()) {
            data.variablenTausch(data.sucheDenNächstenOptimalenTausch());
        }
    }


}
