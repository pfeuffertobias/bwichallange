/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung;

import de.cmtmp.bwichallenge.dataInput.*;
import java.util.*;
import java.util.stream.*;


/**
 * Verarbeitet die Daten
 *
 * @author tobias
 */
public class Verarbeitung {

    private DataInput dataInput;

    /**
     * Initialisiert das Optimierungsobjekt mit einer Default-Datenquelle @see de.cmtmp.bwichallenge.dataInput.DataInputTestData vom Typ <code>DataInput</code>
     */
    public Verarbeitung() {
        //Defauktdaten
        dataInput = new DataInputTestData();
    }

    /**
     * Initialisiert das Optimierungsobjekt mit einer Datenquelle <code>DataInput</code>
     *
     * @param dataInput Die zu verwendende Datenquelle
     */
    public Verarbeitung(DataInput dataInput) {
        this.dataInput = dataInput;
    }

    /**
     *
     * Bildet alle Kombinationen in einer rekursiven Funktion z.B:
     * Eingabe: a,b,c
     * Ausgabe:
     * {
     * a,b,c
     * a,c,b
     * b,a,c
     * b,c,a
     * c,a,b
     * c,b,a
     * }
     *
     * @param list Hieraus werden die Kombinationen gebildet
     *
     * @return Die Kombinationen in einer Liste
     */
    private ArrayList<ArrayList<GewichtDataStorage>> kombinationen(List<GewichtDataStorage> list) {
        ArrayList<ArrayList<GewichtDataStorage>> ret = new ArrayList<>();
        if (list == null || list.size() <= 0) {
            return ret;
        } else if (list.size() == 1) {
            ArrayList<GewichtDataStorage> work0 = new ArrayList<>();
            work0.add(list.get(0));
            ret.add(work0);
            return ret;
        } else if (list.size() == 2) {
            ArrayList<GewichtDataStorage> work0 = new ArrayList<>();
            work0.add(list.get(0));
            work0.add(list.get(1));
            ret.add(work0);
            ArrayList<GewichtDataStorage> work = new ArrayList<>();
            work.add(list.get(1));
            work.add(list.get(0));
            ret.add(work);
            return ret;
        } else {
            for (int localI = 0; localI < list.size(); localI++) {
                ArrayList<GewichtDataStorage> work = new ArrayList<>();
                try {
                    work.addAll(list.subList(0, localI));
                } catch (Exception e) {
                }
                try {
                    work.addAll(list.subList(localI + 1, list.size()));
                } catch (Exception e) {
                }
                ret.addAll(kombinationen(work));
                int hilf = localI;
                ret.stream().forEach(cnsmr -> cnsmr.add(0, list.get(hilf)));
            }
        }

        return ret;
    }

    /**
     * Optimierung mit den im Konstruktor angegebenem Datenset starten (oder den Defaultdaten)
     */
    public void starteTests() {
        //init mit Daten
        ArrayList<TransporterDataStorage> transporterData = dataInput.getTransporterData();
        ArrayList<FahrerDataStorage> fahrerData = dataInput.getFahrerData();

        //Überprüfung auf fehlerhafte Variablenzustände
        assert fahrerData != null : "ArrayList FahrerData ist null";
        assert fahrerData.size() < 0 : "ArrayList FahrerData ist leer";
        assert transporterData != null : "ArrayList TransporterData ist null";
        assert transporterData.size() < 0 : "ArrayList TransporterData ist leer";

        //Speicher init
        List<List<GewichtDataStorage>> store = new ArrayList<>(transporterData.size());
        fahrerData.forEach(_item -> {
            store.add(new ArrayList<>(fahrerData.size()));
        });

        //Möglichkeiten für Fahrer/Transporter Kombinationen
        fahrerData.forEach(fahrer -> {
            int counter = 0;
            for (TransporterDataStorage transporter : transporterData) {
                store.get(counter).add(new GewichtDataStorage(transporter, fahrer));
                counter++;
            }
        });

        //Doppelte Einträge finden
        TreeSet<Integer> löschen = new TreeSet<>();
        for (int localI = 0; localI < store.size(); localI++) {
            for (int localJ = localI; localJ < store.size(); localJ++) {
                boolean equals = store.get(localI).containsAll(store.get(localJ)) && store.get(localJ).containsAll(store.get(localI));//Gibt es einen Doppelten Eintrag
                if (equals) {
                    löschen.add(localJ);//Wenn ja dann vermerke ihn zum löschen
                }
            }
        }
        //Doppelte Einträge löschen
        for (int localI = 0; localI < löschen.size(); localI++) {
            try {
                int pollLast = löschen.pollLast();
                store.remove(pollLast);
            } catch (Exception e) {
                //Es kann eine NullpointerException auftreten sollte löschen leer sein
            }
        }

        //Alle Kombinationen finden, in deren Reihenfolge die Transporter "belaaden" werden sollen
        TestLauf t = new TestLauf(dataInput);
        List<Beladeliste> ret = new ArrayList<>();

        //Bestes Ergebnis finden und ausgeben
        String collect = store.stream()
                .map(fnctn -> kombinationen(fnctn))
                .map(csmr -> csmr.stream()
                .map(localList -> t.testlauf(localList.toArray(new GewichtDataStorage[localList.size()]))))
                .flatMap(x -> x)
                .sorted(Comparator.comparingDouble((Beladeliste keyExtractor) -> keyExtractor.getIndex()).reversed())
                .limit(1)
                .map(testlauf -> testlauf.toString())
                .collect(Collectors.joining("\n------------------------------------------------------------------------------------------------------------------"));


        System.out.println(collect);


    }
}
