/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung;

import de.cmtmp.bwichallenge.dataInput.*;
import java.util.*;
import java.util.stream.*;


/**
 * Erzeugt möglichst optimale Beladelisten für die übergenenen Fahrzeug/Fahrer Gespanne und der Hardware von der DataInput Datenquelle
 *
 * @author tobias
 */
public class TestLauf {

    private DataInput dataInput;

    /**
     *
     * @param paramDataInput Die Datenquelle für die Hardware, die auf die Fahrzeuge verteilt werden soll
     */
    public TestLauf(DataInput paramDataInput) {
        this.dataInput = paramDataInput;
    }


    /**
     *
     * @return Sortierte Liste
     */
    private List<HardwareDataStorage> getHardwareSorted() {
        return dataInput.getHardwareData()
                .sorted(
                        Comparator.
                                comparingDouble(
                                        (HardwareDataStorage t) -> {
                                            return t.berechenNutzwertProGramm();
                                        }
                                ).reversed()
                )
                .collect(Collectors.toList());
    }

    private double möglichstGrosseAnzahlEinpassen(double restgewicht, HardwareDataStorage hardware) {
        double mult = (hardware.getAnzahl() - hardware.getAnzahlVerpackt()) * hardware.getGewicht();
        if (mult <= restgewicht) {
            hardware.setLetzteBeladung(hardware.getAnzahl() - hardware.getAnzahlVerpackt());
            hardware.setAnzahlVerpackt(hardware.getAnzahl());
            return restgewicht - mult;
        } else {
            double rest = restgewicht % hardware.getGewicht();

            int anzahl = (int) Math.floor((restgewicht - rest) / hardware.getGewicht());
            hardware.setLetzteBeladung(anzahl);
            hardware.setAnzahlVerpackt(hardware.getAnzahlVerpackt() + anzahl);

            return restgewicht - (anzahl * hardware.getGewicht());

        }
    }

    private Fahrzeugbeladung transporterBeladen(GewichtDataStorage paramGewichtDataStorage, List<HardwareDataStorage> hardware) {
        //Startladeraum des Transporters
        double restgewicht = paramGewichtDataStorage.getGewicht();

        //Fülle möglichst viele Artikel mit möglichst hohen Nutzwert/Gewicht-Index in den Transporter
        for (HardwareDataStorage localHardwareDataStorage : hardware) {
            restgewicht = möglichstGrosseAnzahlEinpassen(restgewicht, localHardwareDataStorage);
        }

        //Speichere die Beladeliste ab
        Fahrzeugbeladung fahrzeugbeladung = new Fahrzeugbeladung(paramGewichtDataStorage.toString());
        hardware.forEach(localHardwareDataStorage -> {
            fahrzeugbeladung.addBeladung(new HardwareDataStorage(localHardwareDataStorage.getName(), localHardwareDataStorage.getLetzteBeladung(), localHardwareDataStorage.getGewicht(), localHardwareDataStorage.getNutzwert()));
            //reset Speicher für die Anzahl der Artikel im Transporter
            localHardwareDataStorage.resetLetzteBeladung();
        });

        return fahrzeugbeladung;
    }

    /**
     * Erzeugt eine möglicht optimale Beladeliste für die übergenenen Fahrzeug/Fahrer Gespanne in der gegebenen Reihenfolge
     *
     * @param gewichte Die Fahrzeug/Fahrer Gespanne auf die die Hardware verteilt werden soll
     *
     * @return Gibt die best mögliche (nach diesem Algorithmus) Beladeliste der Fahrzeuge zurück
     */
    public Beladeliste testlauf(GewichtDataStorage... gewichte) {
        //init
        List<HardwareDataStorage> hardwareSorted = getHardwareSorted();
        Beladeliste beladeliste = new Beladeliste();

        //Belade nacheinander alle Transporter
        for (GewichtDataStorage localGewichtDataStorage : gewichte) {
            //Kalkuliere die Maximalbeladung
            beladeliste.addFahrzeugBeladung(transporterBeladen(localGewichtDataStorage, hardwareSorted));
        }

        return beladeliste;
    }

}
