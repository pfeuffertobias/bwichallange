/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung.simplex;


/**
 *
 * @author tobias
 */
public class SimplexTabelle {

    private final Object[] basisVariablenNamen;
    private final Object[] unabhängingeVariablenNanme;
    private final double[][] tabelle;//   [basis][unabh]

    public SimplexTabelle(Object[] basisVariablenNamen, double[][] tabelle) {
        this.basisVariablenNamen = basisVariablenNamen;
        this.tabelle = tabelle;

        assert tabelle.length > 0 : "Fehler beim Initialisieren der SimplexTabelle - keine Daten vorhanden";
        this.unabhängingeVariablenNanme = new Object[tabelle[0].length];
        for (int localI = 0; localI < unabhängingeVariablenNanme.length; localI++) {
            unabhängingeVariablenNanme[localI] = "unabhängingeVariable " + localI;
        }
    }

    public boolean isAUnabhängigeVariablePositiv() {
        boolean ret = false;

        //Iteriere durch alle Werte der unabhängingen Variablen und speichere true in ret wenn der überprüfte Wert >0 ist
        for (int localI = 0; localI < tabelle[tabelle.length - 1].length - 1; localI++) {
            if (tabelle[tabelle.length - 1][localI] > 0) {
                ret = true;
            }

        }

        return ret;
    }

    public TauschKoordinaten sucheDenNächstenOptimalenTausch() {
        double größterWert = -1000000000;
        int unabh = -1;
        int basisA = -1;

        for (int localI = 0; localI < tabelle[tabelle.length - 1].length - 1; localI++) {
            if (tabelle[tabelle.length - 1][localI] > 0) {
                int basis = findeTauschpartner(localI);
                if (basis < 0) {
                    return null;
                }
                double wert = tabelle[basis][tabelle[basis].length - 1] / tabelle[basis][0] * tabelle[tabelle.length - 1][localI];

                if (größterWert - wert < 0) {
                    größterWert = wert;
                    unabh = localI;
                    basisA = basis;
                }
            }

        }
        if (unabh < 0) {
            return null;
        }

        return new TauschKoordinaten(basisA, unabh);
    }

    private int findeTauschpartner(int unabh) {
        double klein = Double.MAX_VALUE;
        int letzterKleinsterWert = 0;

        for (int i = 0; i < tabelle.length - 1; i++) {
            if (tabelle[i][0] != 0) {
                double wert = tabelle[i][tabelle[i].length - 1] / tabelle[i][unabh];
                if (klein - wert > 0) {
                    klein = wert;
                    letzterKleinsterWert = i;
                    System.err.println("wert " + wert);
                }
            }
        }
        return letzterKleinsterWert;
    }

    public void variablenTausch(TauschKoordinaten tk) {
        //Validitätscheck
        if (tk == null || tk.getBasis() < 0 || tk.getUnabh() < 0 || tk.getBasis() + 1 >= tabelle.length || tabelle.length <= 0 || tk.getUnabh() + 1 >= tabelle[0].length) {
            System.err.println("Invalide SimplexTabelle oder übergabeparameter");
            return;
        }

        //Tabelle aktualisieren
        zeilenAktiualisieren(tk.getBasis(), tk.getUnabh());

        //Variablennamen tauschen
        Object hilf = basisVariablenNamen[tk.getBasis()];
        basisVariablenNamen[tk.getBasis()] = unabhängingeVariablenNanme[tk.getUnabh()];
        unabhängingeVariablenNanme[tk.getUnabh()] = hilf;
    }

    private void zeilenAktiualisieren(int basis, int unabh) {
        for (int localI = 0; localI < tabelle.length; localI++) {

            //Die Zeile der zu tauschenden Basisvaruable bleibt immer gleich
            if (localI != basis) {
                for (int localJ = 0; localJ < tabelle[localI].length; localJ++) {

                    //Berechne die neuen Werte
                    if (localJ == unabh) {
                        tabelle[localI][localJ] = tabelle[basis][localJ] * tabelle[localI][unabh] * -1;
                    } else {
                        tabelle[localI][localJ] += tabelle[basis][localJ] * tabelle[localI][unabh] * -1;
                    }

                }
            }

        }
    }


    private class TauschKoordinaten {

        private final int basis, unabh;

        public TauschKoordinaten(int paramBasis, int paramUnabh) {
            this.basis = paramBasis;
            this.unabh = paramUnabh;
        }

        /**
         * @return the basis
         */
        public int getBasis() {
            return basis;
        }

        /**
         * @return the unabh
         */
        public int getUnabh() {
            return unabh;
        }


    }


}
