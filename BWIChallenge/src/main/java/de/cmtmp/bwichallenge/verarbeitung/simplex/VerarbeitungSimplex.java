/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung.simplex;

import de.cmtmp.bwichallenge.verarbeitung.*;
import de.cmtmp.bwichallenge.dataInput.*;
import java.util.*;
import java.util.stream.*;


/**
 *
 * @author tobias
 */
public class VerarbeitungSimplex {

    DataInput dataInput;

    /**
     * Initialisiert das Optimierungsobjekt mit einer Default-Datenquelle @see de.cmtmp.bwichallenge.dataInput.DataInputTestData vom Typ <code>DataInput</code>
     */
    public VerarbeitungSimplex() {
        //Defauktdaten
        dataInput = new DataInputTestData();
    }

    /**
     * Initialisiert das Optimierungsobjekt mit einer Datenquelle <code>DataInput</code>
     *
     * @param dataInput Die zu verwendende Datenquelle
     */
    public VerarbeitungSimplex(DataInput dataInput) {
        this.dataInput = dataInput;
    }


    /**
     * Optimierung mit den im Konstruktor angegebenem Datenset starten (oder den Defaultdaten)
     */
    public void starteTests() {
        //init mit Daten
        ArrayList<TransporterDataStorage> transporterData = dataInput.getTransporterData();
        ArrayList<FahrerDataStorage> fahrerData = dataInput.getFahrerData();

        //Überprüfung auf fehlerhafte Variablenzustände
        assert fahrerData != null : "ArrayList FahrerData ist null";
        assert fahrerData.size() < 0 : "ArrayList FahrerData ist leer";
        assert transporterData != null : "ArrayList TransporterData ist null";
        assert transporterData.size() < 0 : "ArrayList TransporterData ist leer";

        //Speicher init
        List<List<GewichtDataStorage>> store = new ArrayList<>(transporterData.size());
        fahrerData.forEach(_item -> {
            store.add(new ArrayList<>(fahrerData.size()));
        });

        //Möglichkeiten für Fahrer/Transporter Kombinationen
        fahrerData.forEach(fahrer -> {
            int counter = 0;
            for (TransporterDataStorage transporter : transporterData) {
                store.get(counter).add(new GewichtDataStorage(transporter, fahrer));
                counter++;
            }
        });

        //Doppelte Einträge finden
        TreeSet<Integer> löschen = new TreeSet<>();
        for (int localI = 0; localI < store.size(); localI++) {
            for (int localJ = localI; localJ < store.size(); localJ++) {
                boolean equals = store.get(localI).containsAll(store.get(localJ)) && store.get(localJ).containsAll(store.get(localI));//Gibt es einen Doppelten Eintrag
                if (equals) {
                    löschen.add(localJ);//Wenn ja dann vermerke ihn zum löschen
                }
            }
        }
        //Doppelte Einträge löschen
        for (int localI = 0; localI < löschen.size(); localI++) {
            try {
                int pollLast = löschen.pollLast();
                store.remove(pollLast);
            } catch (Exception e) {
                //Es kann eine NullpointerException auftreten sollte löschen leer sein
            }
        }

        SimplexAlgorithmus sa = new SimplexAlgorithmus(store.get(0), dataInput);

    }
}
