/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.verarbeitung;

import de.cmtmp.bwichallenge.dataInput.*;


/**
 * Fasst einen Fahrer und ein Fahrzeug zusammen
 * Nur zur vereinfachten Gewichtsberechnung
 *
 * @author tobias
 */
public class GewichtDataStorage {

    private final TransporterDataStorage transporterDataStorage;
    private final FahrerDataStorage fahrerDataStorage;

    /**
     * Ein Fahrzeug und Fahrer werden hier zusammengefasst
     *
     * @param paramTransporterDataStorage Das Fahrzeug (darf nicht null sein)
     * @param paramFahrerDataStorage      Der Fahrer (darf nicht null sein)
     */
    public GewichtDataStorage(TransporterDataStorage paramTransporterDataStorage, FahrerDataStorage paramFahrerDataStorage) {
        assert paramTransporterDataStorage != null : "Fahrzeug darf nicht null sein";
        assert paramFahrerDataStorage != null : "Fahrer darf nicht null sein";
        this.transporterDataStorage = paramTransporterDataStorage;
        this.fahrerDataStorage = paramFahrerDataStorage;
    }


    /**
     * Das Gewicht, dass noch für die Ladung übrigbleibt
     * D.h. das Gewicht des Fahrers wurde schon von der Geamtkapazität abgezogen
     *
     * @return the Gewicht in g
     */
    public double getGewicht() {
        return getTransporterDataStorage().getTransportGewichtInGramm() - getFahrerDataStorage().getGewichtInGramm();
    }

    /**
     * Gibt das verwaltete Fahrzeug zurück
     *
     * @return the transporterDataStorage Fahrzeug des Fahrers
     */
    public TransporterDataStorage getTransporterDataStorage() {
        return transporterDataStorage;
    }

    /**
     * Gibt den verwalteten Fahrer des Fahrzeugs zurück
     *
     * @return the fahrerDataStorage Fahrer des Fahrzeugs
     */
    public FahrerDataStorage getFahrerDataStorage() {
        return fahrerDataStorage;
    }

    @Override
    public boolean equals(Object paramObj) {
        if (paramObj == null) {
            return false;
        }

        if (paramObj instanceof GewichtDataStorage) {
            boolean param1 = ((GewichtDataStorage) paramObj).getGewicht() - getGewicht() <= 0.001;
            return param1;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Transporter kann " + getTransporterDataStorage().getTransportGewicht() + " kg befördern; Fahrer wiegt " + getFahrerDataStorage().getGewicht() + " kg";
    }


}
