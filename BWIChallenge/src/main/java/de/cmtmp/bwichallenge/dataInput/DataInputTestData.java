/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.dataInput;

import java.util.*;
import java.util.stream.*;


/**
 *
 * @author tobias
 */
public class DataInputTestData implements DataInput {

    @Override
    public Stream<HardwareDataStorage> getHardwareData() {
        ArrayList<HardwareDataStorage> hardware = new ArrayList<>(10);

        hardware.add(new HardwareDataStorage("Notebook Büro 13\"", 205, 2451, 40));
        hardware.add(new HardwareDataStorage("Notebook Büro 14\"", 420, 2978, 35));
        hardware.add(new HardwareDataStorage("Notebook Büro outdoor", 450, 3625, 80));
        hardware.add(new HardwareDataStorage("Mobiltelefon Büro", 60, 717, 30));
        hardware.add(new HardwareDataStorage("Mobiltelefon Outdoor", 157, 988, 60));
        hardware.add(new HardwareDataStorage("Mobiltelefon Heavy Duty", 220, 1220, 65));
        hardware.add(new HardwareDataStorage("Tablet Büro klein", 620, 1405, 40));
        hardware.add(new HardwareDataStorage("Tablet Büro groß", 250, 1455, 40));
        hardware.add(new HardwareDataStorage("Tablet outdoor klein", 540, 1690, 45));
        hardware.add(new HardwareDataStorage("Tablet outdoor groß", 370, 1980, 68));

        return hardware.stream();
    }

    @Override
    public ArrayList<TransporterDataStorage> getTransporterData() {
        ArrayList<TransporterDataStorage> transporter = new ArrayList<>(10);

        transporter.add(new TransporterDataStorage(1100));
        transporter.add(new TransporterDataStorage(1100));

        return transporter;
    }

    @Override
    public ArrayList<FahrerDataStorage> getFahrerData() {
        ArrayList<FahrerDataStorage> fahrer = new ArrayList<>(10);

        fahrer.add(new FahrerDataStorage(72.4));
        fahrer.add(new FahrerDataStorage(85.7));

        return fahrer;
    }

}
