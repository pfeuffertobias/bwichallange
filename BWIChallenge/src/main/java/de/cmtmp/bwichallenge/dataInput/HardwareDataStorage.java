/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.dataInput;


/**
 * Speichert alle Daten für eine Hardwareart z.B. Notebook Büro 13"
 *
 * @author tobias
 */
public class HardwareDataStorage {


    private final String name;
    private final double gewicht;
    private final int nutzwert;
    private final int anzahl;

    private int anzahlVerpackt = 0;
    private int letzteBeladung = 0;

    /**
     *
     * @param paramName     Name
     * @param paramAnzahl   Anzahl der durch <code>paramName</code> bestimmten Artikel
     * @param paramGewicht  Gewicht in Gramm
     * @param paramNutzwert Nutzwert eines Artikels
     */
    public HardwareDataStorage(String paramName, int paramAnzahl, double paramGewicht, int paramNutzwert) {
        this.name = paramName;
        this.gewicht = paramGewicht;
        this.nutzwert = paramNutzwert;
        this.anzahl = paramAnzahl;
    }

    @Override
    public String toString() {
        return name + "\t\tAnzahl: " + anzahl + "\tGewicht: " + gewicht + " g\tNutzwert (je Artikel): " + nutzwert;
    }

    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the gewicht
     */
    public double getGewicht() {
        return gewicht;
    }

    /**
     * @return the nutzwert
     */
    public int getNutzwert() {
        return nutzwert;
    }

    /**
     * @return the anzahl
     */
    public int getAnzahl() {
        return anzahl;
    }

    /**
     * @param anzahl the anzahl to set
     */
    public void setAnzahlVerpackt(int anzahl) {
        this.anzahlVerpackt = anzahl;
    }

    /**
     * @return the anzahlVerpackt
     */
    public int getAnzahlVerpackt() {
        return anzahlVerpackt;
    }

    /**
     * @param anzahl the letzteBeladung to set
     */
    public void setLetzteBeladung(int anzahl) {
        this.letzteBeladung = anzahl;
    }

    /**
     * @return the anzahlVerpackt
     */
    public int getLetzteBeladung() {
        return letzteBeladung;
    }

    /**
     * rset letzteBeladung
     */
    public void resetLetzteBeladung() {
        letzteBeladung = 0;
    }


//</editor-fold>
    /**
     * Berechnet den Nutzwert pro Gramm des Artikels
     *
     * @return Nutzwert pro Gramm
     */
    public double berechenNutzwertProGramm() {
        assert gewicht != 0 : "Gewicht des Artikels " + name + " beträgt 0 Gramm! Dies ist kein sinnvoller Wert!";
        assert gewicht < 0 : "Gewicht des Artikels " + name + " ist negativ! Dies ist kein sinnvoller Wert!";
        assert nutzwert < 0 : "Nutzwert des Artikels " + name + " ist negativ! Dies ist kein sinnvoller Wert!";

        return nutzwert / gewicht;
    }

}
