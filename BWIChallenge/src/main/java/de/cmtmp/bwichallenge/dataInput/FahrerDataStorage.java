/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.dataInput;


/**
 * Speichert die Informationen über einen Fahrer
 *
 * @author tobias
 */
public class FahrerDataStorage {

    private final double gewicht;

    /**
     *
     * @param paramGewicht Gewicht des Fahrers in kg
     */
    public FahrerDataStorage(double paramGewicht) {
        this.gewicht = paramGewicht;
    }

    /**
     * @return the gewicht in kg
     */
    public double getGewicht() {
        return gewicht;
    }

    /**
     * @return the gewicht in Gramm
     */
    public double getGewichtInGramm() {
        return gewicht * 1000;
    }


}
