/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.dataInput;


/**
 * Speichert die Informationen über ein Fahrzeug
 *
 * @author tobias
 */
public class TransporterDataStorage {

    private final double transportGewicht;

    /**
     *
     * @param paramTransportGewicht Gewicht dass der Transporter zuladen kann (in Kilogramm)
     */
    public TransporterDataStorage(double paramTransportGewicht) {
        this.transportGewicht = paramTransportGewicht;
    }

    /**
     * @return the transportGewicht in kg
     */
    public double getTransportGewicht() {
        return transportGewicht;
    }

    /**
     * @return the transportGewicht in Gramm
     */
    public double getTransportGewichtInGramm() {
        return transportGewicht * 1000;
    }


}
