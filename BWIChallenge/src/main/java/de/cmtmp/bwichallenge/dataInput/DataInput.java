/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cmtmp.bwichallenge.dataInput;

import java.util.*;
import java.util.stream.*;


/**
 * Die zu bearbeitenden Daten werden hieraus bezogen<br>
 * Implementiere diese Klasse um eine andere Datenquelle außer den Default/Testdaten zu verwenden
 *
 * @author tobias
 */
public interface DataInput {

    /**
     * Methode gibt die zu verarbeitende Hardware als Stream
     * von <code>HardwareDataStorage</code>-Objekten zurück
     *
     * @return Stream von <code>HardwareDataStorage</code>-Objekten
     */
    public Stream<HardwareDataStorage> getHardwareData();

    /**
     * Methode gibt die zu Verfügung stehenden Transporter als ArrayList
     * von <code>TransporterDataStorage</code>-Objekten zurück
     *
     * @return Stream von <code>TransporterDataStorage</code>-Objekten
     */
    public ArrayList<TransporterDataStorage> getTransporterData();

    /**
     * Methode gibt die zu Verfügung stehenden Fahrer als ArrayList
     * von <code>FahrerDataStorage</code>-Objekten zurück
     *
     * @return Stream von <code>FahrerDataStorage</code>-Objekten
     */
    public ArrayList<FahrerDataStorage> getFahrerData();

}
