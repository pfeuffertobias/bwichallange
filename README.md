# BWIChallange - Rucksackproblem

Getestet unter 

	Ubuntu 20.04.1 LTS


mit Java 

	openjdk version "14.0.2" 2020-07-14

	OpenJDK Runtime Environment (build 14.0.2+12-Ubuntu-120.04)

	OpenJDK 64-Bit Server VM (build 14.0.2+12-Ubuntu-120.04, mixed mode, sharing)



----------------------------------------------------------------------------------------
#Build



Build & Run:



```
git clone https://gitlab.com/pfeuffertobias/bwichallange.git

cd bwichallange/BWIChallenge/

mvn package

cd target/

java -jar BWIChallenge-0.0.1.jar
```



----------------------------------------------------------------------------------------




Den gewählten Algorithmus habe ich verwendet, da hier versucht wird, eine möglichst hohe Anzahl an denjenigen Artikeln in die Fahrzeuge zu verladen, die die höchsten Nutzwerte pro Gewicht haben. Somit sollte eine Beladeliste entstehen, die zumindest nicht allzuweit von einer Optimallösung entfernt sein sollte.


Gesamtnutzwert: 74640


Beladeliste:

Fahrzeug: Transporter kann 1100.0 kg befördern; Fahrer wiegt 72.4 kg

	Mobiltelefon Outdoor		Anzahl: 157	Gewicht: 988.0 g	Nutzwert (je Artikel): 60
	Mobiltelefon Heavy Duty		Anzahl: 220	Gewicht: 1220.0 g	Nutzwert (je Artikel): 65
	Mobiltelefon Büro		Anzahl: 60	Gewicht: 717.0 g	Nutzwert (je Artikel): 30
	Tablet outdoor groß		Anzahl: 283	Gewicht: 1980.0 g	Nutzwert (je Artikel): 68
	Tablet Büro klein		Anzahl: 0	Gewicht: 1405.0 g	Nutzwert (je Artikel): 40
	Tablet Büro groß		Anzahl: 0	Gewicht: 1455.0 g	Nutzwert (je Artikel): 40
	Tablet outdoor klein		Anzahl: 0	Gewicht: 1690.0 g	Nutzwert (je Artikel): 45
	Notebook Büro outdoor		Anzahl: 0	Gewicht: 3625.0 g	Nutzwert (je Artikel): 80
	Notebook Büro 13"		Anzahl: 0	Gewicht: 2451.0 g	Nutzwert (je Artikel): 40
	Notebook Büro 14"		Anzahl: 0	Gewicht: 2978.0 g	Nutzwert (je Artikel): 35


Fahrzeug: Transporter kann 1100.0 kg befördern; Fahrer wiegt 85.7 kg

	Mobiltelefon Outdoor		Anzahl: 0	Gewicht: 988.0 g	Nutzwert (je Artikel): 60
	Mobiltelefon Heavy Duty		Anzahl: 0	Gewicht: 1220.0 g	Nutzwert (je Artikel): 65
	Mobiltelefon Büro		Anzahl: 0	Gewicht: 717.0 g	Nutzwert (je Artikel): 30
	Tablet outdoor groß		Anzahl: 87	Gewicht: 1980.0 g	Nutzwert (je Artikel): 68
	Tablet Büro klein		Anzahl: 599	Gewicht: 1405.0 g	Nutzwert (je Artikel): 40
	Tablet Büro groß		Anzahl: 0	Gewicht: 1455.0 g	Nutzwert (je Artikel): 40
	Tablet outdoor klein		Anzahl: 0	Gewicht: 1690.0 g	Nutzwert (je Artikel): 45
	Notebook Büro outdoor		Anzahl: 0	Gewicht: 3625.0 g	Nutzwert (je Artikel): 80
	Notebook Büro 13"		Anzahl: 0	Gewicht: 2451.0 g	Nutzwert (je Artikel): 40
	Notebook Büro 14"		Anzahl: 0	Gewicht: 2978.0 g	Nutzwert (je Artikel): 35
